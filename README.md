## 2023 things to do
- [x] 2024 Software house renewal letter to confirm AGS versions supported - contact AGS secretariat

## 2024 8/7/24 Thins to do

- [ ] Keep help - make it more useful
- [ ] Move table in help to registered organisations
- [ ] Copy member benefits from AGS do to web site.
- [ ] Become a Member - link to page - need to be explicit about different types of membership. 'Data format users'
- [ ] Upgrade to Data Format User - need to clarify wording.
- [ ] Oops message needs to be reworded.
- [ ] Help pages -should this be a blog?
- [ ] AGS4 data format - make version number clearer
- [ ] Roadmap more graphical
- [ ] Need to explore how documentation can be included in Worpress.
- [ ] FAQs
- [ ] Data management


## 2024 things to do
- [x] ELRG Form submission updated from Leon Wordpress (TD)
- [ ] Make 'News' more curent by publishing summary for executive quarterly (JB)
- [ ] Add links to other AGS versions published from round the world (MB)
- [X] Review JSON file problems for website if numeric fields are used - JB decided that using only one file is an issue where new headings and fields may have been proposed and need to be displayed on the website.
- [ ] Close threads more than a year old and make a note of any good ones for publication in AGS Newsletter (MB)
- [ ] Report decent discussion threads for publication in AGS Newsletter
- [ ] Gather website stats if there are any (JB)
- [ ] Review management processes (write them down - finally)
- [ ] Create GitLab guidance on Issues, branches, commits and merges - generate best practice note. (TD)
- [ ] Do user-access permissions work as intended?
- [x] Can we improve presentation on Group Notes within the Groups and heading page (not currently possible).
- [ ] Include Group Rules as per Group Notes?
- [ ] Review each menu item for completeness/updated required.
- [ ] Are there any improvements that aren’t needed, but would be very useful?
- [ ] Review look and feel incl. accessibility (I think it’s terrible and it fails web accessibility checkers) (JB)
- [ ] Roadmap? (Do the AGS have any plans wrt the web site?) - No roadmap that JB is aware of.
- [ ] ELRG/ERES proposed codes in Wordpress not GitLab, proposal date is American again.
- [ ] Ensure issues are tagged correctly. Potential for tasking someone else in DMWG to do this regularly (IL ?)
- [ ] Do we need a 'Pending Corrections' website area? Maintain a list somewhere (Teams or GitLab) and publish this?
- [ ] Link issues with Teams via Channel email address?
- [ ] Add Data Format Registration benefits to the website
- [ ] Update Request forms with AGS version reference 4.0 or 4.1 so we know which version the request applied to, this will involve duplicating the existing 'Submit Code' form.
- [X] Update AGS Piling Page (TD)


# This TODO list is out of date (TD 05/08/2022)

# The web site has been released! The todo list is shown below:

## TD List updated 08/12/2020

## 2021 things to do
- [ ] Include ELRG categories on form - Leon and Paul to provide categories with spreadsheet.
- [ ] Update ELRG table with Leon's spreadsheet.
- [ ] ELRG coding sheet with headings to be sent to Tony from MB PC and LW - provide category lists and some idea of what column headings should be
- [ ] Create the hierarchy document for consumption by developers and other interested parties as a separate entity
- [ ] Instrumentation AGS? Suggest an open format?
- [ ] Smaller window - fix & check menus work Firefox, Chrome on desktop only not phones or Safari/Edge.
- [ ] Romain's HTML tree viewer using javascript reviewer - can we plug that in to the site?
- [ ] Continued Testing
- [ ] Registered users list - on spreadsheet at the moment. Can this be on WP database?
- [ ] Update Request forms with AGS version reference 4.0 or 4.1 so we know which version the request applied to, this will involve duplicating the existing 'Submit Code' form.
- [ ] Create GitLab guidance on Issues, branches, commits and merges - generate best practice note. (TD)
- [ ] create guest account on GitLab to test "reporter" permissions. (TD)

## List updated 16/11/20 TD

- [x] Change titles on ELRG code submission forms from "ERES"
- [X] Contact Gareth about moving staging site to ags.org.uk for us 
- [X] How do we know the background coding is the same in both areas (staging and ags)?
- [X] Reinstate - https://www.ags.org.uk/data-format/?data-format=true - changed by Jackie to https://ags.org.uk/new-data-format/
- [x] Tony to create a migration plan
- [x] Mark to create a line of information for each 'Submit a something page' - one line to describe each page on AGS 4.1 and AGS 4.0
- [x] Ensure the pertinent discussion boards from agsdataformat.com are copied to ags.org.uk
- [x] Create the html file for the hierarchy - TBA
- [x] Update abbr.ags file onto website to include the wireline
- [x] AGS 4.1 dictionary completion to Tony to populate site (JB)
- [X] Update AGS 4.1 welcome page to include words of wisdom from JB! 
- [X] Populate the change log page
- [x] Reminder: ELRG will create a Z code with a number that can be used immediately, P codes will be created if we think it's a good idea and then sent to the proposer to be used going forward, Z code gets deprecated and added to 'AKA column' against P code. Z code will remain even if we think its a bad idea.
- [x] Update ELRG form to include check box to confirm user has checked for codes. (not required)
- [x] Zcodes to be made available immediately to users - TD
- [x] Update 4.1 ELRG to match 4.0 ERES List
- [x] Check ags.org.uk ABBR 4.0,4.1 version filter is replicated on staging site
- [x] Check wpdatatables is the paid for version
- [x] Check consistency between 4.0 and 4.1 page displays
- [x] Insert data types table in Data Types pages TD
- [x] Check traceability of request 4.0 or 4.1 forms
- [x] Ensure that the descriptions for the datatypes match those in the 4.1 document
- [x] Move UNIT.JSON file under AGS 4.0 folder and add to each unit "Version: 4.0,4.1"
- [x] Amend 4.1 'Request an ELRG' page to reflect ELRG and not ERES
- [x] Update the Rules pages on 4.0 to be able to filter on a Rule no. (or a bit of rule text?) - this will need to be a JSON file here.
- [x] Update the Rules pages on 4.1 to be able to filter on a Rule no. (or a bit of rule text?) - this will need to be a JSON file here.
- [x] Change all the reviewed dates in ELRG.JSON to 01/12/2020 as this is the date we think we reviewed it (done this for examples on website. needs to be done again once finalised ELRG ABBR list is available)
- [x] Change the status of unused ERES codes in ELRG (deemed by Paul & Leon) to "DO NOT USE" or "DEPRECATED". (superseded by spreadsheet)
- [x] Add 'Status' and 'Comments' to the UNIT & DATA TYPE form and JSON file - TD 
- [x] update 4.1 request forms to include 4.1 stuff only e.g.  list of groups, currently hardcoded on the form, check each version is correct.


## Testing things to do
- [x] update the code tables to have AGS 4.0 and AGS 4.1 tick boxes so the one table can be used for both AGS 4.1 and AGS 4.0 on the website and wont display new picklist items in the wrong versions.
- [x] Check that switching a company to inactive blocks the members of that company
- [x] Check that the select a company works with the active flag 
- [ ] Test whether the active company flag works for pre-existing approved members and whether their login access is still allowed
- [ ] Test New account Individual Membership - charging £150 not £60
- [ ] Test New account Student Membership
- [x] Test New account Open Source Membership
- [ ] Test New account AGS Trade Member Membership
- [ ] Test New account Commercial Sotware Developer Membership
- [ ] Test New account Non-AGS Trade Member Membership
- [X] Test Access Level individual Membership
- [ ] Test Access Level Student Membership
- [ ] Test Access Level Open Source Membership
- [ ] Test Access Level AGS Trade Member Membership
- [ ] Test Access Level Commercial Sotware Developer Membership
- [ ] Test Access Level Non-AGS Trade Member Membership
- [ ] Test Additional employee Open Source Membership
- [ ] Test Additional employee AGS Trade Member Membership
- [ ] Test Additional employee Commercial Sotware Developer Membership
- [ ] Test Additional employee Non-AGS Trade Member Membership
- [x] Test form submission for ERES codes including messages arriving on gitlab, being sent to DFWebsite group, confirmation we like it, then updating the site (or not) and responding to the sender of the request.
- [x] Test form submission for ABBR codes including messages arriving on gitlab, being sent to DFWebsite group, confirmation we like it, then updating the site (or not) and responding to the sender of the request.
- [x] Test form submission for UNIT codes including messages arriving on gitlab, being sent to DFWebsite group, confirmation we like it, then updating the site (or not) and responding to the sender of the request.
- [x] Test form submission for DICT codes including messages arriving on gitlab, being sent to DFWebsite group, confirmation we like it, then updating the site (or not) and responding to the sender of the request.
- [x] Test form submission for TYPE/UNIT codes including messages arriving on gitlab, being sent to DFWebsite group, confirmation we like it, then updating the site (or not) and responding to the sender of the request.
- [x] Test discussion boards
- [x] Downloads work!
- [x] Navigation of menus and presentaton of navigation options
- [x] New 4.1 menus - populate with words and backend
- [x] Check Code lists are updated
- [x] Check lists up to date
- [x] Re-export dictionary from current AGS website
- [x] Registered User list for 2020 - data entered into AGS Data Tables - how to populate onto the AGS site directly.....Tony? 

## 2020 list

- [x] Restructure the GitLab repo to allow for AGS4.1 etc
- [x] Add version attribute to ABBR file in GitLab
- [x] Test wpDatatables pre-filter for ABBR table (one source - many views)
- [x] Link active registered users in WP to datatable
- [x] Link download files (format documents) held on Wordpress to wpDataTable
- [x] Review menu structure 
- [x] Check status of user if the company has not paid DF subs. Does the code check the status on each login?
- [x] Get details of support contract from AGS (JB)
- [x] Contact Redwire to attend next review meeting (JB)
- [x] Rationalise confirmation emails (TD)
- [x] Change password frequency to every 180 days (JB)
- [x] Email sent from address issue - wordpress@ags.org.uk (JB/TD)
- [x] AGS Trade Member List - not linked to DF - "Upgrade your account" (JB/TD/RW)
- [x] Download 4.0.4 - requesting DF membership, but shouldn't (JB)
- [x] Discussion Forum - change to only approved user (TD)
- [x] Discussion Forum - investigate if can be changed to "approve or denied" user status.(TD)
- [x] Discussion forum - see if full product better for AGS & pay for it. (TD)
- [x] Transfer forum posts
- [x] Wordpress database structure plugins review (TD)
- [x] Wordpress - check backup with AGS (JB)
- [x] Contact AGS to understand governance of web site (JB)
- [x] Investigate how to tick active subscriber (Romain's problem) (JB) 
- [x] Add AGS logo to email notifications (TD) - partly done - some problems showing image.
- [x] Include missing justification on emails (Heading/DICT) (TD)
- [x] Forms - Check all emails/confirmations for consistency (TD)
- [x] ERES code submission process - no code given immediately (TD) - this was implemented previously, but it was decided to take it away.
- [x] Issue created - mail dataformat@ags.org.uk (TD) This cannot be done, but using the service desk can get us closer to the desired outcome.
- [x] Change confirmations and notification email to say review at next meeting - check for consistency (TD)
- [x] Data Format Registration Page - update to iclude "Please login with an activated account" (JB)
- [ ] Create GitLab guidance on Issues, branches, commits and merges - generate best practice note. (TD)
- [x] Merge requests set min number of approvals as 2 - JB, TD, RA and MB can anly approve at the moment (JB)
- [x] ABBR wpdatatable - comment not showing.
- [ ] create guest account on GitLab to test "reporter" permissions. (TD)
- [X] Ask AGS for cpanel access if possible for access to php (JB) 
- [x] Page for AGS secretariat to approve DF users instead of dashboard (JB/RW)
- [x] Form ID 17 Registered users to be populated from WP database, not GitLab data (need to understand database) (JB/TD) Put on hold
- [x] Allow for 4.1 capability on new website. (JB)
- [x] Allow non registered users to download AGS docs (JB/TD)
- [x] Remove ALL data format subscriber roles from everyone on ags.org.uk (JB)
- [x] AGS 4 data format list of documents to be completed (JB?)
- [x] Recheck AGS DF membership - no emails received. (JB)
- [x] Data format Registration connects to AGS Trade Members email domain list (JB)


## Policy decisions/notes

- service desk limited to GitLab users
- use md files for text and json files for data. 
- Keep json files in order
- Issues - only for those with access to action
- Service Desk to be used for forms submission
- Respond to originator through service desk to close the issue
- Issues - responses to come from individuals as comments
- Min no of people to approve merge requests is 2
- Look at subversion governance (MB)
- Open source checker (JB/TD)

## 2019 list

- [x] Config live system
- [x] Transfer pilot config to main site - including url for datatables
- [x] Transfer forum posts
- [x] Sort out content
- [x] Sort out menu structure
- [x] Testing
- [x] Create new website subgroup email for use with gitlab
- [x] Set up gitlab for AGS (dfwg-web@ags.org.uk)
- [x] Set up admin aprovals
- [x] Provide previous versions (4)
- [x] Provide diffs for versions
- [x] create markdown content
- [x] Create P code form
- [x] Get full version of datatables!!!!!
- [x] Put Guidance docs on gitlab
- [x] Get content from Jackie - ERES & ABBR codes done to 26/03/18
- [x] Sort backup for gitlab
- [x] Redirect agsdataformat.com to ags.org.uk/dataformat
- [x] Check with AGS they are happy to authorise DF users permission on website
- [x] Add page to request access to dataformat section of ags.org.uk and send email to ags.org.uk for them to action for us





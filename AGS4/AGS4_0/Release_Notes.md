#AGS 4.0

#Release Notes

The release notes for V4.0 are listed below: 

##February 2017 (AGS 4.0.4)

- The term ‘Moisture content’ has been replaced with the term ‘Water content’ in accordance with BS EN ISO 17892-1:2014.
- PMTD_SEQ heading in the’Pressuremeter Test Results – Individual Loops’ (PMTL) group is marked for deletion.
- New headings are GCHM_DLM, GCHM_RTXT, LOCA_NATD, LOCA_ORID, LOCA_ORJO, LOCA_ORCO, SAMP_RECL and RDEN_IDEN.
- Data types have been changed for CHOC_REF, FRAC_FI, FRAC_IAVE, FRAC_IMAX, IPID_RES, LVAN_VNPK, LVAN_VNRM, RPLT_PLS, RPLT_PLSI, TRIT_CU.
- Window Sampling amended to Dynamic sampling throughout document.Under section 8 Rules, item 8.3 Group Hierarchy, there are 10 groups that are not part of the parent-child hierarchy – PREM was previously missing from the list and has now been included.
- The newly defined headings should be viewed as user defined headings and thus, if used, the DICT group needs to be present in every AGS 4.0.4 submission.
- In response to new laboratory industry standards and updated UK practice the “AGS4 Addendum October 2011” document has been updated.

- The term ‘Moisture content’ has been replaced with the term ‘Water content’ in accordance with BS EN ISO 17892-1:2014. There is no provision for an additional heading for this data. Instead, the data type for every moisture content heading has been changed from ‘MC’ to ‘X’ to allow transmission of results both in the pre-existing and new form to be carried in a file. The headings changed are listed below: 
    - CBRG_NMC, CBRT_MCT, CBRT_MCBT, CBRT_IMC 
    - CMPT_MC 
    - CONG_MCI, CONG_MCF 
    - ESCG_MCI, ESCG_MCF 
    - FRST_MC 
    - ICBR_MC 
    - IDEN_MC 
    - LDEN_MC 
    - LNMC_MC 
    - LPEN_MC 
    - LRES_MC 
    - LSLT_MCI 
    - LVAN_MC 
    - MCVG_NMC, MCVT_MC 
    - PTST_MC 
    - RCCV_MC 
    - RDEN_MC, RDEN_SMC 
    - RWCO_MC 
    - SHBT_MCI, SHBT_MCF 
    - SUCT_MC 
    - TRET_IMC, TRET_FMC 
    - TRIT_IMC, TRIT_FMC 

- PMTD_SEQ heading in the ‘Pressuremeter Test Results – Individual Loops’ (PMTL) group is marked for deletion. It must remain in each AGS 4.0.4 submission as a NULL heading (see Rule 12). 
- A National datum heading LOCA_NATD has been added to the LOCA group. 
- LOCA_ORID, LOCA_ORJO, LOCA_ORCO have been added to facilitate combined archived project data in a data submission. 
- A new heading to record the length of a sample SAMP_RECL has been added to the SAMP group. 
- Fracture spacing headings have a new data type of ‘XN’ to allow both numeric and text data to be transmitted. 
- The following headings have been changed - FRAC_FI, FRAC_IAVE, FRAC_IMAX. 
- CHOC_REF data type has been changed from ID to ‘X’. An ‘ID’ data type implied a uniqueness throughout the entire data submission. 
- LVAN_VNPK and LVAN_VNRM data type has been changed from 0DP to XN to allow text values such as ‘>140’ to be recorded where appropriate. 
- A new heading RDEN_IDEN for Intact Dry Density has been added to the RDEN group. 
- RPLT_PLS and RPLT_PLSI data type changed to 2DP from 1DP. 
- TRIT_CU data type changed to 0DP from 2SF. 
- Window Sampling amended to Dynamic sampling throughout document. 
- IPID_RES data type changed to XN to allow both text and numeric data to be reported. 
- Two new headings have been added to GCHM. GCHM_DLM for the detection limit and GCHM_RTXT for the reported value. 
- In Appendix 3 8.1 Rock Testing Groups listing of RELD Relative Density Test was incorrectly listed and has been moved to 9.1 Soil Testing Groups. 
- Under section 8 Rules, item 8.3 Group Hierarchy, there are 10 groups that are not part of the parent-child hierarchy - PREM was missing from the list and has now been included. 
- All additional headings have been added at the end of the groups to allow users to combine data files from different AGS4 versions without having to re-order headings (see Rule 7). This is especially useful if manual edits are required. 

##October 2011 (4.0.3)

- Section 7 added to provide notes on Internationalization of the AGS Format.
- Specimen depth fields amended to “Depth of top of specimen” to clarify the required data.
- Example added for IRES_TYPE
- SCPG_TYPE example corrected.



##May 2011 (4.0.2)

- Specimen depth fields amended to “Depth of top of specimen” to clarify the required data. 
- CMPG_TESN added to CMPG and CMPT as key
- Heading RCCV_10 corrected to RCCV_100 (RCCV group)
- IPID_RES suggested type changed to 2DP. 
- MONG_BRGC example corrected 
- RCCV_TESN included as key 
- SCDG_CH and SCDG_CHMT added 
- SCPT_TYPE example corrected. 
- SCPT_DPR removed.


##March 2011 (4.0.1)

- Note on associated files added to Section 8.2 Notes on rules. 
- Section 9.3 Units of Measurement section updated/clarified. 
- Ø characters in heading names corrected to 0 characters. 
- uS/cm corrected to μS/cm (LRES and SCPT groups). 
- _REM heading added to CMPT, DETL, FRAC, GEOL, GRAT, MCVT, PLTT, PMTL, SCDT, TRET and TRIT groups. 
- _TESN heading added to laboratory testing groups. To allow for test reference data to be included. 
- CBRG notes for guidance corrected. 
- CHOC_REF updated to a key field (CHOC group). 
- Description of FILE_FSET updated in group descriptions to provide examples of the types of files that could be associated with the data. 
- DICT_UNIT type corrected to PU (DICT group). 
- HDPH group notes for guidance amended. 
- HORN group added to include exploratory hole orientation and inclination data. Same removed from HDPH group. 
- Example for ISPT_METH updated. 
- ISPT notes for guidance extended. 
- LNMC group title amended to Moisture Content Test. LNMC_ISNT added. Notes for guidance updated. 
- LRES_WCND changed to LRES_WRES to express results as water resistivity in ohm m. 
- LSLT_MCI added. 
- MOND group extended to include measurement methods, limits and accreditation information (Headings MOND_METH, MOND_CRED, MOND_LIM, MOND_ULIM added). 
- Heading PMTL_RRM corrected to PMTL_REM (PMTL group). 
- PTST_TYPE type changed to PA and example updated. 
- RDEN_TEMP added. 
- RWCO_TEMP added. 
- TREM_ETIM added. 
- Index extended. 
- Appendix 3 (‘Notes on Specifying AGS Format Data Deliverables’) removed. 


